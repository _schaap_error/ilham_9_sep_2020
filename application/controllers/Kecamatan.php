<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class kecamatan extends CI_Controller {

	function __construct(){
		parent::__construct(); 
		
        //load the department_model
		$this->load->model('DataModel');
		$this->load->library('session');
		$this->load->helper('url');
    }
    
	public function index()
	{
		$data['kecamatan'] = $this->DataModel->get_kecamatan_list();

		$this->load->view('layout/header');
		$this->load->view('layout/topbar');  
		$this->load->view('kecamatan',$data);
		$this->load->view('layout/footer');
	}
	
	public function tambah_kecamatan(){
        
		$this->load->view('layout/header');
		$this->load->view('layout/topbar');  
		$this->load->view('tambah_kecamatan');
		$this->load->view('layout/footer');
	}
	public function edit_kecamatan($id){
		$where=array('id_kecamatan'=>$id);
		$data['kecamatan']=$this->DataModel->edit_kecamatan($where,'kecamatan')->result();
		
		$this->load->view('layout/header'); 
		$this->load->view('layout/topbar');
		$this->load->view('edit_kecamatan',$data);
		$this->load->view('layout/footer');
	}
	
	public function aksi(){
		$id			= $this->input->post('id_kecamatan');
		$kecamatan		= $this->input->post('nama_kecamatan');
		$kota	    = $this->input->post('id_kota');
		$data=array(
			'id_kecamatan'		=> $id,
			'nama_kecamatan'	=> $kecamatan,
			'id_kota'       => $kota

	);
		$this->DataModel->input_data($data,'kecamatan');
		redirect('kecamatan');
	}
	
	public function edit_data(){
		$id			= $this->input->post('id_kecamatan');
		$kecamatan		= $this->input->post('nama_kecamatan');
		$kota	    = $this->input->post('id_kota');
		$data=array(
			'id_kecamatan'		=> $id,
			'nama_kecamatan'	=> $kecamatan,
			'id_kota'       => $kota

	);		
		$where=array(
			'id_kecamatan'=>$id
		);
		
		$this->DataModel->edit_data($where,$data,'kecamatan');
		redirect('welcome');
	}
	
	public function hapus_kecamatan($id){
		$this->input->post('id_kecamatan');
		$this->load->model('DataModel');
		$this->DataModel->hapus_kecamatan($id);
		
		echo $this->session->set_flashdata('hps','Data berhasil dihapus');

		redirect ('welcome/kecamatan');
	}
}