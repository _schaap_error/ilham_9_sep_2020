<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class siswa extends CI_Controller {

	function __construct(){
		parent::__construct(); 
		
        //load the department_model
		$this->load->model('DataModel');
		$this->load->library('session');
		$this->load->helper('url');
    }
    
	public function index()
	{
		$data['siswa'] = $this->DataModel->get_siswa_list();

		$this->load->view('layout/header');
		$this->load->view('layout/topbar');  
		$this->load->view('siswa',$data);
		$this->load->view('layout/footer');
	}
	
	public function tambah_siswa(){
		$this->load->view('tambah_siswa');
	}
	public function edit_siswa($id){
		$where=array('id_siswa'=>$id);
		$data['siswa']=$this->DataModel->edit_siswa($where,'siswa')->result();
		
		$this->load->view('layout/header'); 
		$this->load->view('layout/topbar');
		$this->load->view('edit_siswa',$data);
		$this->load->view('layout/footer');
	}
	
	public function aksi(){
		$id			= $this->input->post('id_siswa');
		$siswa		= $this->input->post('nama_siswa');
		$kota	    = $this->input->post('id_kota');
		$kecamatan	= $this->input->post('id_kecamatan');
		$alamat	    = $this->input->post('alamat');
		$data=array(
			'id_siswa'		=> $id,
			'nama_siswa'	=> $siswa,
			'id_kota'       => $kota,
			'id_kecamatan'  => $kecamatan,
			'alamat'        => $alamat

	);
		$this->DataModel->input_data($data,'siswa');
		redirect('siswa');
	}
	
	public function edit_data(){
		$id			= $this->input->post('id_siswa');
		$siswa		= $this->input->post('nama_siswa');
		$kota	    = $this->input->post('id_kota');
		$kecamatan	= $this->input->post('id_kecamatan');
		$alamat	    = $this->input->post('alamat');
		$data=array(
			'id_siswa'		=> $id,
			'nama_siswa'	=> $siswa,
			'id_kota'       => $kota,
			'id_kecamatan'  => $kecamatan,
			'alamat'        => $alamat

	);		
		$where=array(
			'id_siswa'=>$id
		);
		
		$this->DataModel->edit_data($where,$data,'siswa');
		redirect('siswa');
	}
	
	public function hapus_siswa($id){
		$this->input->post('id_siswa');
		$this->load->model('DataModel');
		$this->DataModel->hapus_siswa($id);
		
		echo $this->session->set_flashdata('hps','Data berhasil dihapus');

		redirect ('siswa');
	}
}