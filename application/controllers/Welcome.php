<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct(){
		parent::__construct();		
		$this->load->model('DataModel');
		$this->load->helper('url');
	} 
	public function index()	
	{

		$this->load->view('layout/header'); 
		$this->load->view('layout/topbar'); 
		$this->load->view('halaman_utama');
		$this->load->view('layout/footer');
	}

	// public function siswa()
	// {
	// 	$data['siswa'] = $this->DataModel->get_siswa_list();

	// 	$this->load->view('layout/header');
	// 	$this->load->view('layout/topbar');  
	// 	$this->load->view('siswa',$data);
	// 	$this->load->view('layout/footer');
	// }

	public function Kecamatan()
	{
		$data['kecamatan'] = $this->DataModel->get_kecamatan_list();

		$this->load->view('layout/header'); 
		$this->load->view('layout/topbar'); 
		$this->load->view('kecamatan',$data);
		$this->load->view('layout/footer');
	}

	public function Kota()
	{
		$data['kota'] = $this->DataModel->get_kota_list();

		$this->load->view('layout/header'); 
		$this->load->view('layout/topbar'); 
		$this->load->view('kota',$data);
		$this->load->view('layout/footer');
	}

}
