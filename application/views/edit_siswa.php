<!-- Begin Page Content -->
<div class="container-fluid">

	<!-- Content Row -->
	<div class="row">

	<!-- Content Column -->
	<div class="col-lg-12 mb-4">

		<!-- Project Card Example -->
		<div class="card shadow mb-4">
		<div class="card-header py-3">
			<h6 class="m-0 font-weight-bold text-primary">Edit Data Siswa</h6>
		</div>
		<div class="card-body">
		<!-- Nested Row within Card Body -->
		<div class="row">
          <div class="col-lg-12">
            <div class="p-5">
            <?php foreach($siswa as $data){ ?>
              <form  action="<?php echo base_url('index.php/siswa/edit_data') ?>" method="post" role="form" class="user">
				<div class="form-group">
                  <input type="text" name="nama_siswa" class="form-control" id=""  value="<?php echo $data->nama_siswa?>">
				</div>
				
				<div class="form-group">
					<select name="id_kota" class="form-control" id="exampleFormControlSelect1">
						<option selected disable><?php echo $data->id_kota?></option>
						<option value="1">Kota Bandung</option>
						<option value="2">Kota Cimahi</option>
						<option value="3">Kabupaten Bandung Barat</option>
					</select>
				</div>
			  
				<div class="form-group">
					<select name="id_kecamatan" class="form-control" id="exampleFormControlSelect1">
						<option selected disable><?php echo $data->id_kecamatan?></option>
						<option name="id_kecamatan" value="1">Antapani</option>
						<option name="id_kecamatan" value="2">Bandung Timur</option>
						<option name="id_kecamatan" value="3">Batujajar</option>
						<option name="id_kecamatan" value="4">Cimahi Selatan</option>
						<option name="id_kecamatan" value="5">Cimahi Tengah</option>
						<option name="id_kecamatan" value="6">Cimahi Utara</option>
						<option name="id_kecamatan" value="7">Lembang</option>
						<option name="id_kecamatan" value="8">Padalarang</option>
					</select>
				</div>

                <div class="form-group">
					<textarea class="form-control" name="alamat" id="" cols="10" rows="5"><?php echo $data->alamat?></textarea>
                </div>
                <button class="btn btn-success"><a class="btn btn-success btn-lg btn-block fas fa-paper-plane text-light"> Edit Siswa</a></button>
                </form>
            <?php } ?>
            </div>
          </div>
        </div>

				</div>
              </div>

            <div class="col-lg-6 mb-4">

            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->
