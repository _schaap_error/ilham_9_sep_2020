<!-- Begin Page Content -->
<div class="container-fluid">

	<!-- Content Row -->
	<div class="row">

	<!-- Content Column -->
	<div class="col-lg-12 mb-4">

		<!-- Project Card Example -->
		<div class="card shadow mb-4">
		<div class="card-header py-3">
			<h6 class="m-0 font-weight-bold text-primary">Tambah Data</h6>
		</div>
		<div class="card-body">
		<!-- Nested Row within Card Body -->
		<div class="row">
          <div class="col-lg-12">
            <div class="p-5">
              <form  action="<?php echo base_url('index.php/siswa/aksi') ?>" method="post" role="form" class="user">
				<div class="form-group">
                  <input type="text" name="nama_siswa" class="form-control" id="" placeholder="Nama Siswa">
				</div>
				
				<div class="form-group">
					<select name="id_kota" class="form-control" id="exampleFormControlSelect1">

					
						<option selected disable>-- Kota / Kabupaten --</option>
						<option value="1">Kota Bandung</option>
						<option value="2">Kota Cimahi</option>
						<option value="3">Kabupaten Bandung Barat</option>
					</select>
				</div>
			  
				<div class="form-group">
					<select name="id_kecamatan" class="form-control" id="exampleFormControlSelect1">
						<option selected disable>-- Kecamatan --</option>
						<option value="1">Antapani</option>
						<option value="2">Bandung Timur</option>
						<option value="3">Batujajar</option>
						<option value="4">Cimahi Selatan</option>
						<option value="5">Cimahi Tengah</option>
						<option value="6">Cimahi Utara</option>
						<option value="7">Lembang</option>
						<option value="8">Padalarang</option>
					</select>
				</div>

                <div class="form-group">
					<textarea class="form-control" name="alamat" id="" cols="10" rows="5">Alamat</textarea>
                </div>
                <button class="btn btn-success"><a class="btn btn-success btn-lg btn-block fas fa-paper-plane text-light"> Tambah Siswa</a></button>
            </div>
          </div>
        </div>

				</div>
              </div>

            <div class="col-lg-6 mb-4">

            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->
