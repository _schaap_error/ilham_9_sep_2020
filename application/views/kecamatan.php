        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Content Row -->
          <div class="row">

            <!-- Content Column -->
            <div class="col-lg-12 mb-4">

              <!-- Project Card Example -->
              <div class="card shadow mb-4">
                <div class="card-header py-3">
                  <h6 class="m-0 font-weight-bold text-primary">Data Kecamatan</h6>
                </div>
                <div class="card-body"> 
					
				<div class="table-responsive">
                <table class="table table-hover" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Nama Kecamatan</th>
                      <th>Kota/Kabupaten</th>
                      <th><a class="btn btn-small text-light" href="<?php echo base_url('index.php/kecamatan/tambah_kecamatan'); ?>"><button type="submit" class="btn btn-success"><i class="fas fa-plus"></i> Tambah</button></a></th>
                    </tr>
                  </thead>
                  <tbody>
          <?php 
          $no = 1;
          foreach ($kecamatan as $kecamatan): 
          ?>
						<tr>
							<td width="50">
								<?php echo $no++ ?>
							</td>
							<td>
								<?php echo $kecamatan->nama_kecamatan ?>
							</td>
							<td>
								<?php echo $kecamatan->nama_kota ?>
                </td>
							<td>
								<!-- <a href="<?php echo base_url('index.php/kecamatan/edit_kecamatan/'.$kecamatan->id_kecamatan) ?>"class="btn btn-small"><i class="fas fa-edit"></i> Edit</a> -->
                <a href="<?php echo base_url('index.php/kecamatan/edit_kecamatan/'.$kecamatan->id_kecamatan)?>"><button class="btn btn-info"><i class="fas fa-edit" aria-hidden="true"></i> Edit</button></a>

                <a onclick="deleteConfirm('<?php echo site_url('kecamatan/delete/'.$kecamatan->id_kecamatan) ?>')"
									href="#!" class="btn btn-small text-danger"><i class="fas fa-trash"></i> Hapus</a>
							</td>
						</tr>
						<?php endforeach; ?>
				</table>
				</div>
				</div>
              </div>

            <div class="col-lg-6 mb-4">

            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->
