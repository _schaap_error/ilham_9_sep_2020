        <!-- Topbar -->
        <nav class="navbar navbar-center navbar-light bg-primary topbar mb-4 static-top shadow ">

            <ul class="navbar-nav col-2">
            <!-- Nav Item - User Information -->
                <li class="nav-item">
                    <a class="btn text-light" href="<?php echo site_url() ?>"><i class="fa fa-home"></i> Beranda</a>
                </li>
            </ul>
            <ul class="navbar-nav col-2">
            <!-- Nav Item - User Information -->
                <li class="nav-item">
                    <a class="btn text-light" href="<?php echo site_url('siswa') ?>"><i class="fa fa-users"></i> Siswa</a>
                </li>
            </ul>
            
            <ul class="navbar-nav col-2">
                <li class="nav-item">
                    <a class="btn text-light" href="<?php echo site_url('kecamatan') ?>"><i class="fa fa-city"></i> Kecamatan</a>
                </li>
            </ul>
            
            <ul class="navbar-nav col-2">
                <li class="nav-item">
                    <a class="btn text-light" href="<?php echo site_url('welcome/kota') ?>"><i class="fa fa-city"></i> Kota / Kab</a>
                </li>
            </ul>

        </nav>
        <!-- End of Topbar -->
        
      <!-- Main Content -->
      <div id="content">

<!-- End of Topbar -->