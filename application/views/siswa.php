        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Content Row -->
          <div class="row">

            <!-- Content Column -->
            <div class="col-lg-12 mb-4">

              <!-- Project Card Example -->
              <div class="card shadow mb-4">
                <div class="card-header py-3">
                  <h6 class="m-0 font-weight-bold text-primary">Data Siswa</h6>
                </div>
                <div class="card-body"> 
					
				<div class="table-responsive">
                <table class="table table-hover" id="dataTable" width="100%" cellspacing="0">
                  <thead class="text-dark">
                    <tr>
                      <th>Id Siswa</th>
                      <th>Nama Siswa</th>
                      <th>Kota/Kabupaten</th>
                      <th>Kecamatan</th>
                      <th>Alamat</th>
                      <th><a class="btn btn-small text-light" href="<?php echo base_url(); ?>"><button type="submit" class="btn btn-success"><i class="fas fa-plus"></i> Tambah</button></a></th>
                    </tr>
                  </thead>
                  <tbody>
					<?php $no=1; foreach ($siswa as $siswa): ?>
						<tr>
							<td width="150">
								<?php echo $no++ ?>
							</td>
							<td>
								<?php echo $siswa->nama_siswa ?>
							</td>
							<td>
								<?php echo $siswa->nama_kota ?>
							</td>
							<td>
								<?php echo $siswa->nama_kecamatan ?>
							</td>
							<td>
								<?php echo $siswa->alamat ?>
							</td>
							<td>
                <a href="<?php echo base_url('index.php/siswa/edit_siswa/'.$siswa->id_siswa)?>"><button class="btn btn-info"><i class="fas fa-edit" aria-hidden="true"></i> Edit</button></a>
                <!-- <a href="< ?php echo site_url('siswa/edit/'.$siswa->id_siswa) ?>" class="btn"><i class="fas fa-edit"></i> Edit</a> -->
                <a href="<?php echo base_url('index.php/siswa/hapus_siswa/'.$siswa->id_siswa);?>"><button class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i> Delete</button></a>
							</td>
						</tr>
						<?php endforeach; ?>
				</table>
				</div>
				</div>
              </div>

            <div class="col-lg-6 mb-4">

            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <div class="modal fade" id="lupa_akun">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Masukkan Email Terdaftar</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="form" method="POST" action="<?php echo base_url() ?>admin/user/lupa_password">
          <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
          <input type="email" name="email" class="form-control" placeholder="Masukkan Email Terdaftar">
          <hr>
          <small>*Anda Akan Menerima Verifikasi Email Untuk Ganti Password</small><br>
          <small>*Kode Verifikasi Berlaku 15 Menit</small><br>
          <small>*Jika Terjadi Pengiriman Kode Verifikasi Ganda, Gunakan Yang Paling Terbaru</small>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-success">Ganti Password!</button>
        </form>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>